package root;

import java.util.*;

public class LinkedList implements List<Integer>{
    private Node head;

    @Override
    public boolean add(Integer number) {
        if (isEmpty()) {
            head = Node.obtain(number);
            return true;
        }

        Node traversalNode = head;

        while (traversalNode.next != null) {
            traversalNode = traversalNode.next;
        }

        traversalNode.next = Node.obtain(number);
        return true;
    }

    @Override
    public boolean remove(Object o) {
        if (isEmpty()) {
            return false;
        }

        Node traversalNode = head;
        if (head.number == (Integer) o) {
            head = traversalNode.next;
            return true;
        }

        while (traversalNode.next != null) {
            if (traversalNode.next.number == (Integer) o) {
                traversalNode.next = traversalNode.next.next;
                return true;
            }
            traversalNode = traversalNode.next;
        }
        return false;
    }

    @Override
    public Integer remove(int index) {
        if (index < 0) {
            throw new IndexOutOfBoundsException();
        }

        Node traversalNode = head;
        Node removedNode;
        if (index == 0) {
            removedNode = head;
            head = traversalNode.next;
            return removedNode.number;
        }

        while (--index != 0) {
            if (traversalNode == null) {
                throw new IndexOutOfBoundsException();
            }
            traversalNode = traversalNode.next;
        }
        removedNode = traversalNode.next;
        traversalNode.next = traversalNode.next.next;
        return removedNode.number;
    }

    @Override
       public void add(int index, Integer number) {
        if (index < 0) {
            throw new IndexOutOfBoundsException();
        }

        Node tempNode;

        if (index == 0) {
            tempNode = head;
            head = Node.obtain(number);
            head.next = tempNode;
            return;
        }

        Node currentNode = head;
        while (--index != 0) {
            if(currentNode == null) {
                throw new IndexOutOfBoundsException();
            }
            currentNode = currentNode.next;
        }

        tempNode = currentNode.next;
        currentNode.next = Node.obtain(number);
        currentNode.next.next = tempNode;
    }

    public int size() {
        int result = 0;
        Node currentNode = head;

        while (currentNode != null) {
            currentNode = currentNode.next;
            result++;
        }
        return result;
    }

    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public boolean contains(Object o) {
        Node traversalNode = head;
        while (traversalNode != null) {
            if (traversalNode.number == (Integer) o) {
                return true;
            }
            traversalNode = traversalNode.next;
        }
        return false;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new Iterator<Integer>() {
            Node node = head;

            public boolean hasNext() {
                return node != null;
            }

            public Integer next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }
                Integer result = node.number;
                node = node.next;
                return result;
            }
        };
    }

    @Override
    public Integer[] toArray() {
        Integer[] result = new Integer[size()];
        int i = 0;
        for (Integer j: this) {
            result[i++] = j;
        }
        return result;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Integer get(int index) {
        if(index < 0) {
            throw new IndexOutOfBoundsException();
        }

        Node traversalNode = head;
        while (index-- != 0) {
            if (traversalNode == null) {
                throw new IndexOutOfBoundsException();
            }
            traversalNode = traversalNode.next;
        }
        return traversalNode.number;
    }

    @Override
    public Integer set(int index, Integer element) {
        if(index < 0) {
            throw new IndexOutOfBoundsException();
        }

        Node traversalNode = head;
        while (index-- != 0) {
            if (traversalNode == null) {
                throw new IndexOutOfBoundsException();
            }
            traversalNode = traversalNode.next;
        }
        Integer result = traversalNode.number;
        traversalNode.number = element;
        return result;
    }

    @Override
    public int indexOf(Object o) {
        int index = -1;
        for (Integer i: this) {
            index++;
            if (i.equals(o)) {
                return index;
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        int index = -1;
        int resultIndex = index;
        for (Integer i: this) {
            index++;
            if (i.equals(o)) {
                resultIndex = index;
            }
        }
        return resultIndex;
    }

    @Override
    public ListIterator<Integer> listIterator() {
        throw new UnsupportedOperationException("implement method listIterator()");
    }

    @Override
    public ListIterator<Integer> listIterator(int index) {
        throw new UnsupportedOperationException("implement method listIterator(int index)");
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        int length = toIndex - fromIndex;
        Node[] result = new Node[length];
        Node traversalNode = head;

        while (fromIndex-- != 0) {
            if (traversalNode == null) {
                throw new ArrayIndexOutOfBoundsException();
            }
            traversalNode = traversalNode.next;
        }

        for (int i = 0; i < length; i++) {
            result[i] = traversalNode;
            traversalNode = traversalNode.next;
        }

        return Arrays.asList(result);
    }


    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            if (!contains(o)) return false;
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends Integer> c) {
        if (isEmpty()) {
            return false;
        }

        Node traversalNode = head;

        while (traversalNode.next != null) {
            traversalNode = traversalNode.next;
        }

        Object[] array = c.toArray();
        for (Object o : array) {
            traversalNode.next = Node.obtain((Integer) o);
            traversalNode = traversalNode.next;
        }

        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends Integer> c) {
        if (index < 0) {
            throw new IndexOutOfBoundsException();
        }

        if (isEmpty()) {
            return false;
        }

        Node tempNode;
        Node traversalNode = head;
        Object[] array = c.toArray();
        if (index == 0) {
            tempNode = head;
            head = Node.obtain((Integer) array[0]);
            traversalNode = head;
            for (int i = 1; i < array.length; i++) {
                traversalNode.next = Node.obtain((Integer) array[i]);
                traversalNode = traversalNode.next;
            }
            traversalNode.next = tempNode;
            return true;
        }

        while (--index != 0) {
            if (traversalNode == null) {
                throw new IndexOutOfBoundsException();
            }
            traversalNode = traversalNode.next;
        }

        tempNode = traversalNode.next;
        for (Object o : array) {
            traversalNode.next = Node.obtain((Integer) o);
            traversalNode = traversalNode.next;
        }
        traversalNode.next = tempNode;
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean result = false;
        for (Object o : c) {
            if(remove(o)) {
                result = true;
            }
        }
        return result;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean result = false;
        for (Integer i : this) {
            if (!c.contains(i)) {
                result = this.remove(i);
            }
        }
        return result;
    }

    @Override
    public void clear() {
        head = null;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("[ ");
        Node currentNode = head;

        while (currentNode != null) {
            builder.append(currentNode.number);
            builder.append(" ");
            currentNode = currentNode.next;
        }

        builder.append("]");
        return builder.toString();
    }

    private static class Node {
        private int number;
        private Node next;

        private static Node obtain(int number) {
            Node node = new Node();
            node.number = number;
            return node;
        }

        @Override
        public String toString() {
            return Integer.toString(number);
        }
    }
}
