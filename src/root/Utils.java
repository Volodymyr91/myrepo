package root;

import java.lang.reflect.Method;
import java.util.Scanner;

public class Utils {
    private static final Scanner SCANNER = new Scanner(System.in);

    public static int inputNumberFromConsole() {
        try {
            return SCANNER.nextInt();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void closeScanner() {
        SCANNER.close();
    }

    public static void printArray(Object[] objects) {
        StringBuilder stringBuilder = new StringBuilder("Array: [ ");
        for (Object i :objects) {
            stringBuilder.append(i + " ");
        }
        stringBuilder.append("]");
        System.out.println(stringBuilder.toString());
    }
}
