package root;

import java.util.ArrayList;

public class Application {
    public static void main(String[] args) {
        LinkedList list = new LinkedList();

        Command command;

        while ((command = Command.getMenuItem()) != Command.QUIT) {
            switch (command) {
                case ADD_NUMBER:
                    list.add(Utils.inputNumberFromConsole());
                    break;
                case ADD_BY_INDEX:
                    list.add(Utils.inputNumberFromConsole(), Utils.inputNumberFromConsole());
                    break;
                case SIZE:
                    System.out.println("Size: " + list.size());
                    break;
                case GET:
                    System.out.println("Got: " + list.get(Utils.inputNumberFromConsole()));
                    break;
                case REMOVE_BY_INDEX:
                    list.remove(Utils.inputNumberFromConsole());
                    break;
                case TO_ARRAY:
                    Utils.printArray(list.toArray());
                    break;
                case INDEX_OF:
                    System.out.println(list.indexOf(Utils.inputNumberFromConsole()));
                    break;
                case CONTAINS:
                    System.out.println(list.contains(Utils.inputNumberFromConsole()) ? "Contains" : "No such element");
                    break;
                case LAST_INDEX_OF:
                    System.out.println(list.lastIndexOf(Utils.inputNumberFromConsole()));
                    break;
                case SUBLIST:
                    System.out.println("Sublist: "
                            + list.subList(Utils.inputNumberFromConsole(), Utils.inputNumberFromConsole()));
                    break;
                case SET:
                    System.out.println(list.set(Utils.inputNumberFromConsole(), Utils.inputNumberFromConsole()));
                    break;
                case CONTAINS_ALL:
                    ArrayList<Integer> arrayList = new ArrayList<>(10);
                    arrayList.add(1);
                    arrayList.add(3);
                    arrayList.add(5);
                    System.out.println(list.containsAll(arrayList));
                    break;
                case ADD_ALL:
                    ArrayList<Integer> arrayList2 = new ArrayList<>(10);
                    arrayList2.add(2);
                    arrayList2.add(12);
                    list.addAll(arrayList2);
                    break;
                case REMOVE:
                    Integer n = Utils.inputNumberFromConsole();
                    list.remove(n);
                    break;
                case REMOVE_ALL:
                    ArrayList<Integer> arrayList3 = new ArrayList<>(10);
                    arrayList3.add(2);
                    arrayList3.add(5);
                    list.removeAll(arrayList3);
                    break;
                case RETAIN_ALL:
                    ArrayList<Integer> arrayList4 = new ArrayList<>(10);
                    arrayList4.add(1);
                    arrayList4.add(15);
                    arrayList4.add(5);
                    list.retainAll(arrayList4);
                    break;
                case ADD_ALL_BY_INDEX:
                    ArrayList<Integer> arrayList5 = new ArrayList<>(10);
                    arrayList5.add(12);
                    arrayList5.add(15);
                    arrayList5.add(51);
                    list.addAll(Utils.inputNumberFromConsole(), arrayList5);
                    break;
            }

            System.out.println(list);
        }

        Utils.closeScanner();
    }

    private enum Command {
        QUIT,
        ADD_NUMBER,
        ADD_BY_INDEX,
        SIZE,
        GET,
        REMOVE_BY_INDEX,
        TO_ARRAY,
        INDEX_OF,
        CONTAINS,
        LAST_INDEX_OF,
        SUBLIST,
        SET,
        CONTAINS_ALL,
        ADD_ALL,
        REMOVE,
        REMOVE_ALL,
        RETAIN_ALL,
        ADD_ALL_BY_INDEX;

        private static final Command[] VALUES = values();

        public static Command getMenuItem() {
            System.out.println("Enter the number of your choice: ");

            for (Command command : VALUES) {
                System.out.println(command.ordinal() + " - " + command.name().toLowerCase());
            }

            System.out.println();
            return inputMenuItem();
        }

        private static Command inputMenuItem() {
            for (;;) {
                int selection = Utils.inputNumberFromConsole();

                if (selection < 0 || selection >= VALUES.length) {
                    System.out.println("incorrect input");
                } else {
                    return VALUES[selection];
                }
            }
        }
    }
}
